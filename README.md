# GA Measurement

Create analytic measurements and send it to the google-analytics server. Find more details regarding GA Measurement Protocols here, https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide.

## Installation

1. Edit your composer.json file and add the following lines:

```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/tannhutha/ga-measurement.git"
        }
    ],
    "require": {
        "tannhutha/ga-measurement": "^1.0.0"
    }
}
```

2. Run composer install or composer update

```
composer install
```
```
composer update
```

## Currently Supported Tracking Types

1. Tannhutha\GAMeasurement\Measurements\Event([...])
2. Tannhutha\GAMeasurement\Measurements\Page([...])
3. Tannhutha\GAMeasurement\Measurements\Social([...])

## Basic Usage

```
<?php
require('/path/to/vendor/autoload.php');
```

### Event Tracking

#### Parameters excerpt from Google document

https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#event

- ec  // Event Category. Required.
- ea  // Event Action. Required.
- el  // Event label.
- ev  // Event value.

```
$ga = Tannhutha\GAMeasurement\Analytics::create(
    new Tannhutha\GAMeasurement\Measurements\Event([
        'ec' => 'Contact Us',
        'ea' => 'Onsite Click',
        'el' => 'Email',
        'ev' => '0'
    ])
)->forPropertyId('UA-130000000-1');

$response = $ga->commit();
```

### Page Tracking

#### Parameters excerpt from Google document

https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#page

- dh    // Document hostname.
- dp    // Page.
- dt    // Title.

```
$ga = Tannhutha\GAMeasurement\Analytics::create(
    new Tannhutha\GAMeasurement\Measurements\Page([
        'dh' => 'mydemo.com',
        'dp' => '/home',
        'dt' => 'homepage'
    ])
)->forPropertyId('UA-130000000-1');

$response = $ga->commit();
```

### Social Tracking

#### Parameters excerpt from Google document

https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#social

- sa    // Social Action. Required.
- sn    // Social Network. Required.
- st    // Social Target. Required.

```
$ga = Tannhutha\GAMeasurement\Analytics::create(
    new Tannhutha\GAMeasurement\Measurements\Social([
        'sa' => 'like',
        'sn' => 'facebook',
        'st' => '/home'
    ])
)->forPropertyId('UA-130000000-1');

$response = $ga->commit();
```

## Response

boolean true for success and false for failure.

### Error Handling

Errors are store as an Exception object $e.

```
$ga = Tannhutha\GAMeasurement\Analytics::create(
    new Tannhutha\GAMeasurement\Measurements\Event([
        'ec' => 'Contact Us',
        'ea' => 'Onsite Click',
        'el' => 'Email',
        'ev' => '0'
    ])
)->forPropertyId('A-BAD-PROPERTY-ID');

if(!$response = $ga->commit()){
    echo $ga->e->getCode();     // 400
    echo $ga->e->getMessage();  // [VALUE_INVALID] The value provided for parameter 'tid' is invalid. Please see http://goo.gl/a8d4RP#tid for details.
}
```
