<?php
namespace Tannhutha\GAMeasurement;

abstract class Measurement{

    const VERSION = '1';
    const DEF_CLIENT_ID = 'de6cea76-971b-4ee5-b05f-177f6012207b';
    const GA_ENDPOINT_URL = 'https://www.google-analytics.com/collect';

    protected $params;

    public abstract function getType(): string;

    public function __construct(array $userDefined){
        $this->params = new \Adbar\Dot();
        // Protocol version
        $this->params->set('v', self::VERSION);
        // "event", "pageview", "transaction", "social", "exception", "timing", "screenview"
        // The most common is "event"
        $this->params->set('t', $this->getType());
        // set user defined parameters pertaining to the tracking type
        foreach($userDefined as $k => $v){
            $this->params->set($k, $v);
        }
        // Clied ID (cid) is required if User ID (uid) is not specified
        if(!$this->params->has('uid') && !$this->params->has('cid')){
            $this->params->set('cid', self::DEF_CLIENT_ID);
        }
    }

    public function __toString(){
        return \http_build_query($this->params->get());
    }

    public function getParams(){
        return $this->params->get();
    }
}