<?php
namespace Tannhutha\GAMeasurement\Measurements;

use Tannhutha\GAMeasurement\Measurement;

Class PageView extends Measurement{
    
    public function getType(): string {
        return $type = 'pageview';
    }
}