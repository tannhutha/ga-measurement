<?php
namespace Tannhutha\GAMeasurement;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;

class Analytics{

    const GA_DEBUG_ENDPOINT_URL = 'https://www.google-analytics.com/debug/collect';

    private static $instance = null;
    private $gaPropertyId = null;
    private $gaMeasurement = null;
    public $e = null;

    private function __construct(\Tannhutha\GAMeasurement\Measurement $measurement){
        $this->gaMeasurement = $measurement;
    }

    public static function create($measurement){
        self::$instance = new self($measurement);

        return self::$instance;
    }

    public function forPropertyId($gaPropertyId){
        $this->gaPropertyId = $gaPropertyId;

        return $this;
    }

    public function validate(){
        $body = (string) $this->gaMeasurement . "&tid={$this->gaPropertyId}";
        $request = new Request('POST', self::GA_DEBUG_ENDPOINT_URL, [
            'cache-control' => 'no-cache',
            'Connection' => 'keep-alive',
            'Accept-Encoding' => 'gzip, deflate',
            'Accept' => '*/*',
            'User-Agent' => $_SERVER['HTTP_USER_AGENT'],
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => \strlen($body)
        ], $body);
        $httpClient = new HttpClient();
        try{
            $response = $httpClient->send($request);
            $responseData = json_decode((string) $response->getBody(), true);
            if(isset($responseData['hitParsingResult'][0]['valid']) && $responseData['hitParsingResult'][0]['valid'] === true){
                return true;
            }else{
                $errors = [];
                foreach($responseData['hitParsingResult'][0]['parserMessage'] as $message){
                    \array_push($errors, "[{$message['messageCode']}] {$message['description']}");
                }
                $errorLong = \join(', ', $errors);
                $this->e = new \Exception($errorLong, 400);
            }
        } catch (ClientException $e) {
            $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
        } catch (RequestException $e) {
            $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
        } catch (ConnectException $e) {
            $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
        } catch (BadResponseException $e) {
            $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
        } catch (ServerException $e) {
            $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
        } catch (TooManyRedirectsException $e) {
            $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
        }
        return false;
    }

    public function commit(){
        if($this->validate()){
            $body = (string) $this->gaMeasurement . "&tid={$this->gaPropertyId}";
            $request = new Request('POST', \Tannhutha\GAMeasurement\Measurement::GA_ENDPOINT_URL, [
                'cache-control' => 'no-cache',
                'Connection' => 'keep-alive',
                'Accept-Encoding' => 'gzip, deflate',
                'Accept' => '*/*',
                'User-Agent' => $_SERVER['HTTP_USER_AGENT'],
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Content-Length' => \strlen($body)
            ], $body);
            $httpClient = new HttpClient();
            try{
                $response = $httpClient->send($request);
                if($response->getStatusCode() === 200){
                    return true;
                }
            } catch (ClientException $e) {
                $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
            } catch (RequestException $e) {
                $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
            } catch (ConnectException $e) {
                $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
            } catch (BadResponseException $e) {
                $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
            } catch (ServerException $e) {
                $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
            } catch (TooManyRedirectsException $e) {
                $this->e = new \Exception("{$e->getResponse()->getStatusCode()} {$e->getResponse()->getReasonPhrase()}", $e->getResponse()->getStatusCode());
            }
        }
        return false;
    }
}